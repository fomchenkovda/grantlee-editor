# Translation of contactprintthemeeditor.po to Catalan
# Copyright (C) 2015-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2015, 2016, 2017, 2019, 2020, 2021, 2022.
# Josep M. Ferrer <txemaq@gmail.com>, 2018, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: grantlee-editor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-15 00:39+0000\n"
"PO-Revision-Date: 2022-03-27 11:57+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Antoni Bella"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "antonibella5@yahoo.com"

#: contactprintthemeconfiguredialog.cpp:32
#, kde-format
msgctxt "@title:window"
msgid "Configure"
msgstr "Configuració"

#: contactprintthemeconfiguredialog.cpp:43
#, kde-format
msgid "Default contact:"
msgstr "Contacte per omissió:"

#: contactprintthemeconfiguredialog.cpp:49
#, kde-format
msgid "General"
msgstr "General"

#: contactprintthemeconfiguredialog.cpp:52
#, kde-format
msgid "Default Template"
msgstr "Plantilla per omissió"

#. i18n: ectx: Menu (edit)
#: contactprintthemeeditorui.rc:16
#, kde-format
msgid "&Edit"
msgstr "&Edita"

#. i18n: ectx: Menu (Display)
#: contactprintthemeeditorui.rc:22
#, kde-format
msgid "&Display"
msgstr "&Mostra"

#: editorpage.cpp:55
#, kde-format
msgid "Theme Templates:"
msgstr "Plantilles de temes:"

#: main.cpp:22 main.cpp:24
#, kde-format
msgid "Contact Print Theme Editor"
msgstr "Editor de temes per a imprimir els contactes"

#: main.cpp:26
#, kde-format
msgid "Copyright © 2015-%1 contactprintthemeeditor authors"
msgstr "Copyright © 2015-%1, Els autors del contactprintthemeeditor"

#: main.cpp:27
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:27
#, kde-format
msgid "Maintainer"
msgstr "Mantenidor"

#: themeeditormainwindow.cpp:76
#, kde-format
msgid "Load Recent Theme..."
msgstr "Carrega un tema recent..."

#: themeeditormainwindow.cpp:83
#, fuzzy, kde-format
#| msgid "Add Extra Page..."
msgctxt "@action"
msgid "Add Extra Page..."
msgstr "Afegeix una pàgina extra..."

#: themeeditormainwindow.cpp:87
#, kde-format
msgid "Upload theme..."
msgstr "Publica un tema..."

#: themeeditormainwindow.cpp:93
#, kde-format
msgid "New theme..."
msgstr "Tema nou..."

#: themeeditormainwindow.cpp:96
#, kde-format
msgid "Open theme..."
msgstr "Obre un tema..."

#: themeeditormainwindow.cpp:98
#, kde-format
msgid "Save theme"
msgstr "Desa el tema"

#: themeeditormainwindow.cpp:101
#, kde-format
msgid "Save theme as..."
msgstr "Desa el tema com a..."

#: themeeditormainwindow.cpp:107
#, fuzzy, kde-format
#| msgid "Install theme"
msgctxt "@action"
msgid "Install theme"
msgstr "Instal·la un tema"

#: themeeditormainwindow.cpp:111
#, fuzzy, kde-format
#| msgid "Insert File..."
msgctxt "@action"
msgid "Insert File..."
msgstr "Insereix un fitxer..."

#: themeeditormainwindow.cpp:115
#, fuzzy, kde-format
#| msgid "Manage themes..."
msgctxt "@action"
msgid "Manage themes..."
msgstr "Gestiona els temes..."

#: themeeditormainwindow.cpp:119
#, kde-format
msgid "Update view"
msgstr "Actualitza la vista"

#: themeeditormainwindow.cpp:189 themeeditormainwindow.cpp:322
#, kde-format
msgid "Select theme directory"
msgstr "Selecciona el directori del tema"

#: themeeditormainwindow.cpp:205
#, kde-format
msgid "Directory does not contain a theme file. We cannot load theme."
msgstr ""
"El directori no conté un fitxer de tema. No s'ha pogut carregar el tema."

#: themeeditorpage.cpp:40
#, kde-format
msgid "Editor (%1)"
msgstr "Editor (%1)"

#: themeeditorpage.cpp:46
#, kde-format
msgid "Desktop File"
msgstr "Fitxer d'escriptori"

#: themeeditorpage.cpp:134
#, kde-format
msgid "Theme already exists. Do you want to overwrite it?"
msgstr "El tema ja existeix. El voleu sobreescriure?"

#: themeeditorpage.cpp:135
#, kde-format
msgid "Theme already exists"
msgstr "El tema ja existeix"

#: themeeditorpage.cpp:143
#, kde-format
msgid "Cannot create theme folder."
msgstr "No s'ha pogut crear la carpeta del tema."

#: themeeditorpage.cpp:153
#, kde-format
msgid "Theme installed in \"%1\""
msgstr "Tema instal·lat a «%1»"

#: themeeditorpage.cpp:174
#, kde-format
msgid "We cannot add preview file in zip file"
msgstr "No s'ha pogut afegir un fitxer de vista prèvia al fitxer ZIP"

#: themeeditorpage.cpp:174
#, kde-format
msgid "Failed to add file."
msgstr "Ha fallat en afegir el fitxer."

#: themeeditorpage.cpp:188
#, kde-format
msgid "My favorite KMail header"
msgstr "La meva capçalera preferida del KMail"

#: themeeditorpage.cpp:211
#, kde-format
msgid "Filename of extra page"
msgstr "Nom de fitxer de la pàgina extra"

#: themeeditorpage.cpp:211
#, kde-format
msgid "Filename:"
msgstr "Nom de fitxer:"

#: themeeditorpage.cpp:257
#, kde-format
msgid "Do you want to save current project?"
msgstr "Voleu desar el projecte actual?"

#: themeeditorpage.cpp:258
#, kde-format
msgid "Save current project"
msgstr "Desa el projecte actual"

#: themetemplatewidget.cpp:35
#, kde-format
msgid "You can drag and drop element on editor to import template"
msgstr ""
"Podeu arrossegar i deixar anar elements sobre l'editor per a importar una "
"plantilla"
